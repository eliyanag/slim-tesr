<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;

$app = new \Slim\App();
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
$app->get('/customers/{number}', function($request, $response,$args){
   return $response->write('Your customer number is '.$args['number']);
});
$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Your customer number is '.$args['cnumber'].', and your product is '.$args['pnumber']);
});

$app->get('/messages', function($request, $response,$args){
    $_message = new Message();
    $messages = $_message->all();

    $payload = [];
    foreach($messages as $msg){
        $payload[$msg->id] = [
            "body" => $msg->body,
            "user_id" => $msg->user_id,
            "created_at" => $msg->created_at
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $userid = $request->getParsedBodyParam('user_id','');
    $_message = new Message();
    $_message->body = $message;
    $_message->user_id = $userid;
    $_message->save();
    
    if($_message->id){
        $payload = ['message_id' => $_message->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->delete('/messages/{message_id}', function($request, $response,$args){
    $message = Message::find($args['message_id']);
    $message->delete();

    if($message->exists){
        return $response->withStatus(400);
    }
    else{
        return $response->withStatus(200);
    }
});
//שליפת נתונים
$app->get('/users', function($request, $response,$args){
    $_user = new User();
    $users = $_user->all();

    $payload = [];
    foreach($users as $u){
        $payload[$u->id] = [
            "username" => $u->username,
            "email" => $u->email
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//עדכון
$app->post('/users', function($request, $response,$args){
    $email = $request->getParsedBodyParam('email','');
    $username = $request->getParsedBodyParam('username','');
    $_user = new User();
    $_user->email = $email;
    $_user->username = $username;
    $_user->save();
       
    if($_user->id){
        $payload = ['user_id' => $_user->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});
$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $_message = Message::find($args['message_id']);
    $_message->body = $message;
   
    if($_message->save()){ //אם הערך נשמר
        $payload = ['message_id' => $_message->id,"result" => "The message has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

//מחיקה
$app->delete('/users/{user_id}', function($request, $response,$args){
    $user = User::find($args['user_id']);
    $user->delete();
    
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});
$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();

    Message::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});



$app->run();
